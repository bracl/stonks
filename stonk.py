#!/usr/bin/env python3

import requests
import yfinance
from bs4 import BeautifulSoup

import pprint
import json

from symbols import symbols

session = requests.session()


class Stock:
    def __init__(self, symbols):
        if isinstance(symbols, str):
            self.y_symbol = symbols
            self.r_symbol = symbols
        elif isinstance(symbols, list):
            self.y_symbol = symbols[0]
            self.r_symbol = symbols[1]
        self.yf = yfinance.Ticker(self.y_symbol)
        try:
            self.info = self.yf.info
            self.name = self.info['longName']
            print(f'Successful in returning info for {self.y_symbol} - {self.name}')
        except KeyError as ke:
            self.info = {}
            print(f'Failed to get information on {self.y_symbol} - {ke}')
        except ValueError as ve:
            self.info = {}
            print(f'Failed to get information on {self.y_symbol} - {ve}')

    def pretty(self):
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(self.info)

    def yahoo_finance(self):
        url = f'https://uk.finance.yahoo.com/quote/{self.y_symbol}/analysis'
        text = session.get(url=url).content
        soup = BeautifulSoup(text, features="lxml")
        h = soup.find_all('script')
        j = {}
        for script in h:
            if script.string and "root.App.main" in script.string:
                data = script.string.split('main =')[1][:-10].strip()[:-1]  # hacky
                j = json.loads(data)
                break
        return j

    def reuters(self):
        url = f'https://www.reuters.com/companies/{self.r_symbol}'
        text = session.get(url=url).content
        soup = BeautifulSoup(text, features="lxml")
        h = soup.find_all('script')
        j = {}
        left = None
        slider = soup.find('div', {'class': 'RangeLine-marker-4X5wq RangeLine-measured-21ZF5'})
        if slider:
            left = float(slider.attrs['style'].split('left:')[1][:-1])
        for script in h:
            if 'id' in script.attrs and script.attrs['id'] == '__NEXT_DATA__':
                j = json.loads(script.string)
        return j, left

    def price(self):
        bid = round(self.info['bid'], 2)
        ask = round(self.info['ask'], 2)
        return (round(bid + ask, 3)) / 2


def target_price(jayson):
    try:
        if 'context' in jayson:
            return jayson['context']['dispatcher']['stores']['QuoteSummaryStore']['financialData']['targetMeanPrice'][
                'raw']
        elif 'props' in jayson:
            return jayson['props']['initialState']['markets']['profile']['keystats']['recommendation'][
                'preliminary_mean']
    except KeyError as ke:
        return None
    except TypeError as te:
        # print(te)
        return None


nad = 'No Analyst Data'


def what_do(value):
    if value:
        if value <= 25:
            return 'Sell Sell Sell'
        elif 25 < value <= 45:
            return 'Sell'
        elif 45 < value <= 60:
            return 'Hold'
        elif 60 < value <= 80:
            return 'Buy'
        elif 80 < value:
            return 'To the moon !!'
    else:
        return nad


if __name__ == '__main__':
    stonks = []
    fails = []
    for s in symbols:
        st = Stock(s)
        if st.info:
            stonks.append(st)
        else:
            fails.append(s)
    for s in stonks:
        price = s.price()
        yf = s.yahoo_finance()
        r, left = s.reuters()
        print(s.name, s.y_symbol)
        for prediction in [yf, r]:
            target = target_price(prediction)
            analyst = target if target else nad
            if 'context' in prediction:
                print(f'\tYahoo')
                print(f'\t\tCurrent: {price}')
                print(f'\t\tTarget Price: {analyst}')
            if 'props' in prediction:
                wd = what_do(left)
                print(f'\tReuters')
                print(f'\t\tRecommendation Mean: {left}')
                print(f'\t\tWhat do?: {wd}')

    print(fails)
